<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\{
    DashboardController,
    LoginController, 
    RegisterController,
    ForgotPasswordController,
    ResetPasswordController,
    ProfileController,
    SocketController,
    UserController,
    InboxController,
    FileController,
    NotificationController,
    JobPostController,
    ApplicationController
};

Route::post('login', [LoginController::class, 'index']);
Route::prefix('register')->group(function () {
    Route::post('/', [RegisterController::class, 'register']);
    Route::post('otp', [RegisterController::class, 'registerWithOTP']);
    Route::post('verify/send-otp', [RegisterController::class, 'sendOTPCode']);
    Route::post('verify', [RegisterController::class, 'verifyOTPCode']);
});
Route::post('forgot-password', [RegisterController::class, 'index']);
Route::post('reset-password', [ResetPasswordController::class, 'index'])->middleware('guest');

Route::middleware(['auth:sanctum'])->group(function () {
    
    Route::post('broadcasting/auth', [SocketController::class, 'broadcastingPresenceAuth']);

    Route::get('dashboard', [DashboardController::class, 'index']);

    Route::prefix('notifications')->group(function () {
        Route::get('/', [NotificationController::class, 'index']);
        Route::get('count', [NotificationController::class, 'count']);
    });

    Route::prefix('inbox')->group(function () {
        Route::get('/', [InboxController::class, 'index']);
        Route::get('/unread', [InboxController::class, 'unread']);
        Route::post('/create', [InboxController::class, 'create']);
        Route::get('/show/{conversation_id}', [InboxController::class, 'show']);
        Route::post('/send/{conversation_id}', [InboxController::class, 'send']);
    });

    Route::prefix('job-posts')->group(function () {
        Route::get('/', [JobPostController::class, 'index']);
        Route::post('create', [JobPostController::class, 'create']);
        Route::post('update/{id}', [JobPostController::class, 'update']);
        Route::post('delete', [JobPostController::class, 'destroy']);
    });

    Route::prefix('applications')->group(function () {
        Route::get('/', [ApplicationController::class, 'index']);
        Route::post('create', [ApplicationController::class, 'create']);
        Route::post('update/{id}', [ApplicationController::class, 'update']);
        Route::post('delete', [ApplicationController::class, 'destroy']);
    });

    Route::prefix('files')->group(function () {
        Route::post('upload', [FileController::class, 'upload']);
    });

    Route::prefix('user')->group(function () {
        Route::prefix('profile')->group(function () {
            Route::get('/', [ProfileController::class, 'index']);
            Route::prefix('update')->group(function () {
                Route::post('/', [ProfileController::class, 'update']);
                Route::post('profile-photo', [ProfileController::class, 'updateProfilePhoto']);
                Route::post('availability-status', [ProfileController::class, 'updateAvailabilityStatus']);
                Route::post('emergency-contact', [ProfileController::class, 'updateEmergencyContact']);
                Route::post('password', [ProfileController::class, 'updatePassword']);
            });
        });
    });

    Route::prefix('users')->group(function () {
        Route::get('role/{role_id}', [UserController::class, 'index']);
        Route::get('show/{id}', [UserController::class, 'show']);
        Route::post('update/{id}', [UserController::class, 'update']);
        Route::post('delete', [UserController::class, 'destroy']);
        Route::post('identification/status/{id}', [UserController::class, 'identificationStatus']);
    });
});
