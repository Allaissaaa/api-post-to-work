<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{ BelongsTo };
use App\Traits\FormatDates;

class Application extends Model
{
    use HasFactory, FormatDates;

    protected $fillable = [
        'user_id',
        'job_post_id',
        'status',
    ];

    protected $appends = [
        'created_at_formatted',
    ];

    public function getCreatedAtFormattedAttribute()
    {
        if (empty($this->attributes['created_at'])) {
            return null;
        }

        return $this->getFormatDate($this->attributes['created_at'], 'M d, Y g:i A');
    }
    
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function jobPost(): BelongsTo
    {
        return $this->belongsTo(JobPost::class);
    }
}
