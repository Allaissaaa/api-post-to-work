<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{ BelongsTo, HasMany };
use App\Traits\FormatDates;

class JobPost extends Model
{
    use HasFactory, FormatDates;

    protected $fillable = [
        'user_id',
        'company_information_id',
        'title',
        'description',
        'number_of_vacancy',
        'job_type',
        'status',
    ];

    protected $appends = [
        'created_at_formatted',
    ];

    public function getCreatedAtFormattedAttribute()
    {
        if (empty($this->attributes['created_at'])) {
            return null;
        }

        return $this->getFormatDate($this->attributes['created_at'], 'M d, Y g:i A');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function companyInformation(): BelongsTo
    {
        return $this->belongsTo(CompanyInformation::class);
    }

    public function applications(): HasMany
    {
        return $this->hasMany(Application::class);
    }
}
