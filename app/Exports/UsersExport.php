<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{
    protected $roleId;

    public function __construct($roleId) 
    {
        $this->roleId = $roleId;
    }

    public function view(): View
    {
        return view((int) $this->roleId === 2 ? 'exportables.drivers' : 'exportables.users', [
            'roleId' => $this->roleId,
            'users' => User::where(['role_id' => $this->roleId])->get(),
        ]);
    }
}
