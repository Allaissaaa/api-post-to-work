<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\{ User };
use App\Services\Notification;
use App\Services\SemaphoreSms;

class UserController extends Controller
{
    public function index($roleId, Request $request) 
    {
        $data = User::with([])->where(['role_id' => $roleId])->orderBy('id', 'DESC');

        if ($request->has('search') && $request->search !== '') {
            $data->where(function ($q) use ($request) {
                $q->where('first_name', 'like', '%'.$request->search.'%')
                ->orWhere('last_name', 'like', '%'.$request->search.'%');
            });
        }

        return $data->paginate($request->per_page ?? 10);
    }

    public function show($id)
    {
        return User::with([])->find($id);
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        
        $user->update($request->all());

        if ($request->hasFile('photo')) {
            $s3 = Storage::disk('s3');
            $fileUrl = $s3->url($s3->put('files/avatar/'.$user->id, $request->photo, 'public'));
            $user->update(['photo' => $fileUrl]);
        }

        return response()->json([
            'message' => 'Updated Successfully.',
            'success' => true,
        ], 200); 
    }

    public function destroy(Request $request)
    {
        $data = User::find($request->id);
        
          if ($data && $data->delete()) {
              return response()->json([
                  'message' => 'Deleted Successfully.',
                  'success' => true,
              ], 200);
          }

          return response()->json([
              'message' => 'Failed.',
              'success' => false,
          ], 200);
    }
}