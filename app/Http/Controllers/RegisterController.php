<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use DB;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use App\Services\Authentication;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\{ RegisterRequest, VerifyContactNoRequest, VerificationCodeRequest };
use App\Helpers\Utils;
use Carbon\Carbon;

class RegisterController extends Controller
{
    private $authentication;

    public function __construct(
        Authentication $authentication
    )
    {
        $this->authentication = $authentication;
    }

    public function index(Request $request)
    {
        return view('auth.register', $request->all());
    }

    public function register(RegisterRequest $request)
    {
        return response()->json([
            'message' => 'Registration details are valid.',
            'success' => false,
        ], 200);
    }

    public function registerWithOTP(RegisterRequest $request)
    {
        $verification = $this->authentication->generateTokenCodeForSMS($request->contact_no);

        return response()->json([
            'sms_token' => $verification['token'],
            'message' => 'OTP Sent Successfully.',
            'success' => true,
        ], 200);
    }

    public function sendOTPCode(VerifyContactNoRequest $request)
    {
        $verification = $this->authentication->generateTokenCodeForSMS($request->contact_no);

        return response()->json([
            'token' => $verification['token'],
            'message' => 'OTP Sent Successfully.',
            'success' => true,
        ], 200);
    }

    public function verifyOTPCode(VerificationCodeRequest $request)
    {
        $cacheKey = 'verification-sms.'.$request->token;
        $verification = Cache::get($cacheKey);

        if (! $verification || $verification['code'] !== $request->code) {
            throw ValidationException::withMessages(['code' => 'Invalid or expired code.']);
        }

        $request->merge([
            'is_active' => true,
            'status' => 'Pending',
            'status' => 'Active',
            'contact_no' => $request->contact_no, 
            'contact_no_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password),
        ]);

        $user = User::create($request->except(['token', 'code', 'password_confirmation']));
        $token = $user->createToken('API TOKEN');

        return response()->json([
            'user' => User::with([])->find($user->id),
            'token' => $token->plainTextToken,
            'message' => 'Contact Number Verified Successfully.',
            'success' => true,
        ], 200);
    }
}
