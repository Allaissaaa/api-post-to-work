<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ JobPost };
use App\Http\Requests\JobPostRequest;

class JobPostController extends Controller
{
    public function index(Request $request)
    {
        $data = JobPost::with(['user', 'companyInformation'])->withCount(['applications'])->orderBy('id', $request->sort_by ?? 'DESC');
        
        if(auth()->user()->role->slug === 'client') {
            $data->where(['user_id' => $request->user()->id]);
        }

        if ($request->ajax() || ($request->has('search') && $request->search !== '')) {
            $data->where('title', 'like', '%'.$request->search.'%');
        }

        return $data->paginate($request->per_page ?? 10);
    }

    public function create(JobPostRequest $request)
    {
        JobPost::create($request->all());

        return response()->json([
            'redirect_url' => redirect()->back()->getTargetUrl(),
            'message' => 'Created Successfully.',
            'success' => true,
        ], 200); 
    }

    public function update($id, JobPostRequest $request)
    {
        JobPost::find($id)->update($request->all());

        return response()->json([
            'redirect_url' => redirect()->back()->getTargetUrl(),
            'message' => 'Updated Successfully.',
            'success' => true,
        ], 200); 
    }

    public function destroy(Request $request)
    {
        $data = JobPost::find($request->id);

        if ($data && $data->delete()) {
            return response()->json([
                'redirect_url' => redirect()->back()->getTargetUrl(),
                'message' => 'Successfully Deleted',
                'success' => true,
            ], 200);
        }

        return response()->json([
            'message' => 'Theres an error deleting this item.',
            'success' => false,
        ], 200);
    }
}
