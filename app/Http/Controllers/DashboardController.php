<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ User, Booking, Transaction, Payment, Withdrawal, DeclineCancelBooking };
use App\Traits\ChartData;
use Carbon\Carbon;

class DashboardController extends Controller
{
    use ChartData;

    public function index(Request $request)
    {
        return [
            'drivers' => User::drivers()->count(),
            'passengers' => User::users()->count(),
            'bookings' => Booking::count(),
            'payments' => Payment::count(),
            'withdrawals' => Withdrawal::count(),
            'recent_bookings' => Booking::with(['user', 'driver', 'driver.driverInformation', 'origin', 'destination'])->latest()->take(5)->get(),
            'booking_chart' => $this->adminChartData(8),
            'drivers_ontrip' => User::drivers()->count(),
            'successful_bookings_today' => Booking::whereDate('created_at', Carbon::today())->count(),
            'total_cancelled_bookings' => DeclineCancelBooking::count(),
        ];
    }
}
