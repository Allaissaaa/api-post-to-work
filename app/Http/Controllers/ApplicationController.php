<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Application };
use App\Http\Requests\ApplicationRequest;

class ApplicationController extends Controller
{
    public function index(Request $request)
    {
        $data = Application::orderBy('id', $request->sort_by ?? 'DESC');
                
        if ($request->ajax() || ($request->has('search') && $request->search !== '')) {
            $data->where('id', 'like', '%'.$request->search.'%');
        }

        return $data->paginate($request->per_page ?? 10);
    }

    public function create(ApplicationRequest $request)
    {
        Application::create($request->all());

        return response()->json([
            'redirect_url' => redirect()->back()->getTargetUrl(),
            'message' => 'Created Successfully.',
            'success' => true,
        ], 200); 
    }

    public function update($id, ApplicationRequest $request)
    {
        Application::find($id)->update($request->all());

        return response()->json([
            'redirect_url' => redirect()->back()->getTargetUrl(),
            'message' => 'Updated Successfully.',
            'success' => true,
        ], 200); 
    }

    public function destroy(Request $request)
    {
        $data = Application::find($request->id);

        if ($data && $data->delete()) {
            return response()->json([
                'redirect_url' => redirect()->back()->getTargetUrl(),
                'message' => 'Successfully Deleted',
                'success' => true,
            ], 200);
        }

        return response()->json([
            'message' => 'Theres an error deleting this item.',
            'success' => false,
        ], 200);
    }
}
