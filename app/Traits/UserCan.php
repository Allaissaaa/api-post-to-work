<?php

namespace App\Traits;

trait UserCan
{
    public function canCreateProduct()
    {
        return auth()->user()->status == 'Active';
    }

    public function canCreateBid()
    {
        return auth()->user()->status == 'Active';
    }
}
