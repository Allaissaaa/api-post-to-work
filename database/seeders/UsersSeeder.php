<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{ User, CompanyInformation, JobPost };
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Helpers\Utils;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'is_super' => 1,
            'role_id' => 1,
            'status' => 'Active',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('09071995'),
            'email_verified_at' => Carbon::now(),
        ]);
        User::create([
            'is_super' => 0,
            'role_id' => 2,
            'status' => 'Active',
            'first_name' => 'Client',
            'last_name' => 'Client',
            'email' => 'client@test.com',
            'password' => Hash::make('09071995'),
            'email_verified_at' => Carbon::now(),
        ]);
        User::create([
            'is_super' => 0,
            'role_id' => 3,
            'status' => 'Active',
            'first_name' => 'Frelancer',
            'last_name' => 'Frelancer',
            'email' => 'frelancer@test.com',
            'password' => Hash::make('09071995'),
            'email_verified_at' => Carbon::now(),
        ]);
        User::factory()
        ->count(5)
        ->create(['role_id' => 1, 'is_super' => 1]);
        User::factory()
        ->count(5)
        ->create(['role_id' => 1, 'is_super' => 0]);
        User::factory()
        ->hasCompanyInformations(5)
        ->hasJobPosts(10, function (array $attributes, User $user) {
            return ['company_information_id' => 1];
        })
        ->count(50)
        ->create(['role_id' => 2]);
        User::factory()
        ->count(50)
        ->create(['role_id' => 3]);
    }
}
