<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('company_information_id')->constrained('company_information')->onUpdate('cascade')->onDelete('cascade');
            $table->string('status')->default('Inactive'); // Active, Inactive
            $table->text('title')->nullable();
            $table->longText('description')->nullable();
            $table->integer('number_of_vacancy')->default(0);
            $table->text('job_type')->nullable(); // Part Time / Full Time
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
