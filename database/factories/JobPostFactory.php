<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class JobPostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(2),
            'description' => $this->faker->paragraph(6),
            'number_of_vacancy' => random_int(0, 10),
            'job_type' => 'Regular',
            'status' => 'Active',
        ];
    }
}
