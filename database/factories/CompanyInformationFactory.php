<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyInformationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(2),
            'description' => $this->faker->paragraph(6),
            'address' => $this->faker->address,
            'contact_no' => '09'.$this->faker->randomNumber(9),
            'email' => $this->faker->unique()->safeEmail(),
            'status' => 'Active',
        ];
    }
}
