<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => 'Active',
            'first_name' => $this->faker->firstName,
            'last_name' =>  $this->faker->lastName,
            'address' => $this->faker->address,
            'contact_no' => '09'.$this->faker->randomNumber(9),
            'gender' => array_rand(['Male', 'Female']),
            'date_of_birth' => Carbon::now()->format('Y-m-d'),
            'contact_no_verified_at' => Carbon::now(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => Hash::make('09071995'),
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
