<nav id="sidebar">
  <div class="p-3 d-flex justify-content-center align-items-center flex-column">
    <img src="{{ Auth::user()->photo }}" width="70" height="70" class="rounded-circle mx-2 bg-secondary mb-3 border">
    <h6>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h6>
    <p class="mb-0 small">{{ Auth::user()->role->name }}</p>
  </div>
  <ul class="nav nav-pills flex-column mb-auto px-2 side-navigator">
    <li>
      <a href="{{ route('dashboard.index') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'dashboard.index' ? 'active' : '' }}">
        <i class="fas fa-columns" style="width: 18px;"></i>
        <span class="ms-1">Dashboard</span>
      </a>
    </li>
    <li>
      <a href="{{ route('user.list', 'passengers') }}" class="nav-link link-dark w-100 {{ (Route::currentRouteName() === 'user.list' && request()->type === 'passengers') ? 'active' : '' }}">
        <i class="fas fa-users" style="width: 18px;"></i>
        <span class="ms-1">Passengers</span>
      </a>
    </li>
    <li>
      <a href="{{ route('user.list', 'riders') }}" class="nav-link link-dark w-100 {{ (Route::currentRouteName() === 'user.list' && request()->type === 'riders') ? 'active' : '' }}">
        <i class="fas fa-users" style="width: 18px;"></i>
        <span class="ms-1">Riders</span>
      </a>
    </li>
    <li>
      <a href="{{ route('bookings.list') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'bookings.list' ? 'active' : '' }}">
        <i class="fas fa-motorcycle" style="width: 18px;"></i>
        <span class="ms-1">Booking History</span>
      </a>
    </li>
    <li>
      <a href="{{ route('transactions.list') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'transactions.list' ? 'active' : '' }}">
        <i class="fas fa-file" style="width: 18px;"></i>
        <span class="ms-1">Transactions</span>
      </a>
    </li>
    <li>
      <a href="{{ route('emergency-hotlines.list') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'emergency-hotlines.list' ? 'active' : '' }}">
        <i class="fa fa-phone" style="width: 18px;"></i>
        <span class="ms-1">Emergency Hotlines</span>
      </a>
    </li>
    <li>
      <a href="#" class="nav-link link-dark w-100 d-none {{ Route::currentRouteName() === '' ? 'active text-white' : '' }}">
        <i class="fa-solid fa-money-bill" style="width: 18px;"></i>
        <span class="ms-1 small">Payments</span>
      </a>
    </li>
    <li>
      <a href="{{ route('announcements.list') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'announcements.list' ? 'active' : '' }}">
        <i class="fas fa-bullhorn" style="width: 18px;"></i>
        <span class="ms-1">Announcements</span>
      </a>
    </li>
    <li class="d-none">
      <a href="{{ route('faqs.list') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'faqs.list' ? 'active' : '' }}">
        <i class="fa fa-question" style="width: 18px;"></i>
        <span class="ms-1">FAQ</span>
      </a>
    </li>
    <li>
      <a href="{{ route('profile.index') }}" class="nav-link link-dark w-100 {{ Route::currentRouteName() === 'profile.index' ? 'active' : '' }}">
        <i class="fa fa-user" style="width: 18px;"></i>
        <span class="ms-1">Profile</span>
      </a>
    </li>
    <li>
      <a href="{{ route('logout') }}" class="nav-link link-dark w-100" onclick="event.preventDefault(); document.getElementById('logout-form-2').submit();">
        <i class="fa-solid fa-right-from-bracket" style="width: 18px;"></i>
        <span class="ms-1">Logout</span>
      </a>
    </li>
  </ul>
  <form id="logout-form-2" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
  </form>
</nav>