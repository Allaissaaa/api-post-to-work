@extends('layouts.app-no-nav')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 offset-md-4 d-flex align-items-center justify-content-center">
      <div class="card bg-transparent border-0 w-100">
        <div class="card-body">
          <div class="text-center bg-transparent border-0">
            <img src="{{ asset('images/logo.png') }}" width="300">
          </div>
          <h3>Login</h3>
          <form method="POST" action="{{ route('login') }}" data-ajax="true">
            @csrf
            <div class="form-floating mb-3">
              <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="{{ __('Email Address') }}">
              <label for="email" class="form-label">{{ __('Email Address') }}</label>
              @error('email')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-floating mb-3">
              <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password" placeholder="{{ __('Password') }}">
              <label for="password" class="form-label">{{ __('Password') }}</label>
              @error('password')
              <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-check d-none">
              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label class="form-check-label" for="remember">
              {{ __('Remember Me') }}
              </label>
            </div>
            <div class="mb-4 d-none">
              <a class="btn btn-link p-0 btn-lg" href="{{ route('password.request') }}">
              {{ __('Forgot Your Password?') }}
              </a>
            </div>
            <div class="d-grid gap-2">
              <button type="submit" class="btn btn-primary btn-lg mb-5">
              {{ __('Login') }}
              </button>
             </div>
            <div class="text-center d-none">
              <p>Don't have an account? <a href="{{ route('register') }}">Register</a></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
