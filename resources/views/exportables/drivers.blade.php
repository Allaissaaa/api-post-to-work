<table>
    <thead>
    <tr>
        <th>#ID</th>
        <th>Verification Status</th>
        <th>Plate Number</th>
        <th>Name</th>
        <th>Franchise Status</th>
        <th>No Travel</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->driverInformation->status ?? 'N/A' }}</td>
            <td>{{ $user->driverInformation->plate_number ?? 'N/A' }}</td>
            <td>{{ $user->first_name }} {{ $user->last_name }}</td>
            <td>{{ ! empty($user->driverInformation->franchise) ? 'Registered' : 'Unregistered' }}</td>
            <td>{{ $user->riderBookings()->count() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>