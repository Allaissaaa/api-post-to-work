<table>
    <thead>
    <tr>
        <th>#ID</th>
        <th>Name</th>
        <th>No Travel</th>
        <th>Success</th>
        <th>Cancelled</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->first_name }} {{ $user->last_name }}</td>
            <td>{{ $user->bookings()->count() }}</td>
            <td>{{ $user->successBookings()->count() }}</td>
            <td>{{ $user->cancelledBookings()->count() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
