@extends('layouts.app-no-nav')
@section('content')
<div class="container">
<div class="vh-100">
  <div>
    <div class="row">
      <div class="col-md-6 offset-md-3 vh-100 d-flex justify-content-center align-items-center">
        <div class="text-center">
          <h1 class="mb-3 text-capitalize">Payment {{$status}}</h1>
          <p class="">You can now close this page</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection